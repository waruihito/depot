Rails.application.routes.draw do
scope '(:locale)' do
  get 'admin' => 'admin#index'
  controller :sessions do
    get 'login' => :new

    post 'login' => :create

    delete "logout" =>:destroy
 end

 #devise_for :users

  resources :user1s
  #devise_for :installs
  resources :orders
  resources :line_items
  resources :carts
  get 'store/index'

  resources :products do
  	get :who_bought, on: :member
  end

  root to: 'store#index', as:'store'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end #end locale
end
