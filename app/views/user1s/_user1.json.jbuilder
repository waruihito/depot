json.extract! user1, :id, :name, :password_digest, :created_at, :updated_at
json.url user1_url(user1, format: :json)
