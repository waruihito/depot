class AdminController < ApplicationController
 skip_before_filter :authorize
 #before_action :set_cart, only: [:index]
 def index
  	@total_orders = Order.count
 	@cart = current_cart
  end
end
