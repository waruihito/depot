class User1sController < ApplicationController
  skip_before_filter :authorize
  before_action :set_user1, only: [:show, :edit, :update, :destroy]
  before_action :sida123
  # GET /user1s
  # GET /user1s.json
  def index
    @user1s = User1.order(:name)
    respond_to do |format|
    format.html # index.html.erb 
    format.json { render json: @users }
end
  end

  # GET /user1s/1
  # GET /user1s/1.json
  def show
  end

  # GET /user1s/new
  def new
    @user1 = User1.new
  end

  # GET /user1s/1/edit
  def edit
  end

  # POST /user1s
  # POST /user1s.json
  def create
    @user1 = User1.new(user1_params)

    respond_to do |format|
      if @user1.save
        format.html { redirect_to user1s_url, notice: 'User1 #{@user1.name } was successfully created.' }
        format.json { render :show, status: :created, location: @user1 }
      else
        format.html { render :new }
        format.json { render json: @user1.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user1s/1
  # PATCH/PUT /user1s/1.json
  def update
    respond_to do |format|
      if @user1.update(user1_params)
        format.html { redirect_to user1s_url, notice: 'User1 #{@user1.name }  was successfully updated.' }
        format.json { render :show, status: :ok, location: @user1 }
      else
        format.html { render :edit }
        format.json { render json: @user1.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user1s/1
  # DELETE /user1s/1.json
  def destroy
    begin 
        @user1.destroy
        flash[:notice] = "User #{@user1.name} delete"
    rescue Exception => e
        flash[:notice] = e.message
    end

    respond_to do |format|
      format.html { redirect_to user1s_url, notice: "Can't delete User " }
      format.json { head :no_content }
    end
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user1
      @user1 = User1.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user1_params
      params.require(:user1).permit(:name, :password, :password_confirmation)
    end
    def sida123
      @cart = current_cart
    end
     #wrap_parameters :user, include: [:name, :password, :password_confirmation]

end
