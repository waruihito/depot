class SessionsController < ApplicationController
  skip_before_filter :authorize

  def new
    @cart = current_cart
  end

  def create
  	user = User1.find_by_name(params[:name])
  	if user and user.authenticate(params[:password])
  		session[:user1_id] = user.id
  		redirect_to admin_url
  	else
  		redirect_to login_url, arlet:" Invalid user/password"
  	end
  end

  def destroy
  	session[:user1_id] = nil
  	redirect_to store_url, notice: 'logged out'
  end
end
