class Product < ApplicationRecord
  #attr_accessible :description, :image_url, :price, :title
  has_many :line_items
  has_many :orders, through: :line_items


  
  before_destroy :check_product
  validates :title, :description, :image_url, presence: true
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
# 
  validates :title, uniqueness: true
  validates :image_url, allow_blank: true, format: {
    with:    %r{\.(gif|jpg|png)\Z}i,
    message: 'must be a URL for GIF, JPG or PNG image.'
  }
  def who_bought
    @product = Product.find(params[:id])
    respond_to do |format|
      format.atom
    end
  end
  private
  #Kiem tra truoc khi xoa product
  def check_product
  	if line_items.empty?
  		return true
  	else
  		errors.add(:base, 'Line Items present')
  		return false
  	end

  end
end
