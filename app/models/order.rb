class Order < ApplicationRecord
	PAYMENT_TYPES = ['Check', 'Credit Card', 'Purchase order']
	validates :name, :email, :address, presence: true
	validates :pay_type, inclusion: PAYMENT_TYPES
	has_many :line_items, dependent: :destroy
	def add_line_items_form_cart(cart)
		cart.line_items.each do |item|
			item.cart_id = nil
			line_items << item
		end
	end
end
