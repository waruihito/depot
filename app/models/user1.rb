class User1 < ApplicationRecord
after_destroy :not_delete_user_if_user_only
has_secure_password
validates :name, presence: true, uniqueness: true 
def not_delete_user_if_user_only
	if User1.count.zero?
		raise "Can't delete last user"
	end
end
end
