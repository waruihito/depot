class OrderNotifierMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier_mailer.received.subject
  #
  default from: 'quocbao.ha@citynow.vn'
  
  def received(order)
    @order = order
    #mail (to: @order.mail, subject: "Confirmation")    
    mail(to: @order.email, subject: "Confirmation")

  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier_mailer.shipped.subject
  #
  def shipped(order)
    @order = order

    mail to: order.email,subject:  "to@example.org"
  end
end
